# Mainzelliste Docker

This is the official Mainzelliste docker image. It is currently in BETA stage and not yet intended for production purposes.

According to Docker best practices, the Mainzelliste docker instance (subdirectory `mainzelliste`) relies on a database running elsewhere (e.g. another container or the host machine).

## Using Mainzelliste Docker with docker-compose

The easiest way to run Mainzelliste via Docker is using `docker-compose`, which will provide both Mainzelliste and a database.

To do so, switch to the root of this repository and run:
```
docker-compose up [-d]
```

To stop:
```
docker-compose down
```

This will run Mainzelliste with default values.

## Using Mainzelliste without docker-compose

If you have an existing database for Mainzelliste to use, simply run the container as follows:

```
docker run -e ML_DB_HOST=mydb -e ML_DB_PASS=secretpassword medicalinformatics/mainzelliste
```

For more environment variables, see the next section.

## Configuration

Mainzelliste docker accepts the following configuration parameters:

* `ML_DB_DRIVER` Database JDBC driver; default: `org.postgresql.Driver`
* `ML_DB_TYPE` Database JDBC type; default: `postgresql`
* `ML_DB_HOST` Database hostname; default: `db`
* `ML_DB_PORT` Database port; default: `5432`
* `ML_DB_NAME` Database name; default: `mainzelliste`
* `ML_DB_USER` Database user; default: `mainzelliste`
* `ML_DB_PASS` Database pass; default: `mainzelliste`

## Known issues for Windows users

* There is a bug in the official postgres image when using Docker for Windows that prevents postgres from starting, see https://github.com/docker/for-win/issues/445 and `docker-compose.yml`.
